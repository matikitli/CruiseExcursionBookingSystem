package com.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;


@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)

public class SpringSecurityConfig extends WebSecurityConfigurerAdapter{

    private static final String[] AUTH_IGNORE_SWAGGER_API = {
            "/v2/api-docs",
            "/swagger-resources",
            "/swagger-resources/**",
            "/configuration/ui",
            "/configuration/security",
            "/swagger-ui.html",
            "/webjars/**"};

    private static final String[] AUTH_IGNORE = {"/api/customers/register",
                                                    "/api/customers/loginPerformed",
                                                    "/api/customers/logoutPerformed"
    };

    private static final String[] AUTH_ONLY_ADMIN = {"/api/bookings/findAll", "/api/bookings/findAllByExcursionId", "/api/bookings/findAllByDateTrip",
                                                        "/api/customers/findAll", "/api/trips/create", "/api/trips/delete", "/api/waitlists/findAll",
                                                        "/api/waitlists/findAllByTripId"
    }; //probably we will use pre request verification

    private static final String[] AUTH_ADMIN_USER = {"/api/bookings/findOneById", "/api/bookings/findAllByCustomerId", "/api/bookings/findOneByCustomerIdExcursionIdDate",
                                                        "/api/bookings/create", "/api/bookings/update", "/api/bookings/delete", "/api/customers/current",
                                                        "/api/customers/findOne", "/api/customers/update","/api/customers/delete",
                                                        "/api/excursions/findOne", "/api/excursions/findAll", "/api/trips/findAll", "/api/trips/findOneById",
                                                        "/api/trips/findAllByExcursionId", "/api/trips/findAllByDate", "/api/waitlists/findAllByCustomerId",
                                                        "/api/waitlists/findOneById", "/api/waitlists/findOneByTripIdCustomerId"
    };

    @Autowired
    private CurrentCustomerSecurityDetailsService currentCustomerSecurityDetailsService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.csrf().disable();

        http.authorizeRequests()
                .antMatchers(AUTH_IGNORE).permitAll()
                .antMatchers(AUTH_IGNORE_SWAGGER_API).permitAll()
                .antMatchers(AUTH_ADMIN_USER).hasAnyAuthority(Role.USER.toString().toUpperCase(), Role.ADMIN.toString().toUpperCase())
                .antMatchers( AUTH_ONLY_ADMIN ).hasAnyAuthority( Role.ADMIN.toString().toUpperCase() )
                .anyRequest().fullyAuthenticated()
                .and().formLogin()
                .loginPage("/api/customers/login") //POST !
                .usernameParameter("email") //email in database (aa@bb.cc)
                .passwordParameter("password") //password in db (1234)
                .successForwardUrl("/api/customers/loginPerformed")
                //TODO: create failure handler
                .permitAll()
                .and()
                .logout()
                .logoutUrl("/api/customers/logout")
                .logoutSuccessUrl("/api/customers/logoutPerformed")
                .deleteCookies()
                .permitAll();
    }

    @Override
    public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder.userDetailsService(currentCustomerSecurityDetailsService);
    }


}
