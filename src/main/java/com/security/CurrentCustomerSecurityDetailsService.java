package com.security;

import com.models.Customer;
import com.services.booking.BookingService;
import com.services.customer.CustomerService;
import com.services.trip.TripService;
import com.services.waitList.WaitListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;

@Service
public class CurrentCustomerSecurityDetailsService implements UserDetailsService {

    @Autowired
    CustomerService customerService;

    @Autowired
    private HttpSession httpSession;

    @Autowired
    private BookingService bookingService;

    @Autowired
    private WaitListService waitListService;

    @Override
    public CurrentCustomer loadUserByUsername(String email) throws UsernameNotFoundException {
        Customer customer = customerService.getByEmail(email)
                .orElseThrow(()->new UsernameNotFoundException("User not found"));
        return new CurrentCustomer(customer);
    }

    public CurrentCustomer getCurrentCustomer() throws Exception {
        CurrentCustomer customer = (CurrentCustomer) httpSession.getAttribute("current");
        if(customer==null){
            throw new Exception("You are not logged in");
        }
        return customer;
    }

    public void logoutPerformed(){
        httpSession.removeAttribute("current");
    }

    public boolean canAccess(String typeOfEntity, Long idOfEntity) throws Exception {
        Customer current = getCurrentCustomer().getCustomer();
        if(current.getRole().equals(Role.ADMIN)){
            return true;
        }
        switch (typeOfEntity.toUpperCase()){
            default: return false;
            case "BOOKING":
                if(bookingService.findBookingById(idOfEntity).getCustomer().getId().equals(current.getId())){
                    return true;
                }
                return false;
            case "CUSTOMER":
                if(idOfEntity.equals(current.getId())){
                    return true;
                }
                return false;
            case "WAITLIST":
                if(waitListService.findOneById(idOfEntity).getCustomerId().equals(current.getId())){
                    return true;
                }
                return false;
        }

    }

}
