package com.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpSession;

@Component
public class LoggedCustomerListener implements ApplicationListener<AuthenticationSuccessEvent> {

    @Autowired
    private HttpSession httpSession;

    @Override
    public void onApplicationEvent(AuthenticationSuccessEvent event) {
        CurrentCustomer currentCustomer = (CurrentCustomer) event.getAuthentication().getPrincipal();
        System.out.println("Logged successful as: " + currentCustomer.getCustomer().getName());
        httpSession.setAttribute("current", currentCustomer);
    }
}
