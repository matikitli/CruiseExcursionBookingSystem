package com.security;

public enum Role {
    USER,
    ADMIN;
}
