package com.security;

import com.models.Customer;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

public class CurrentCustomer extends User {



    private Customer customer;

    public CurrentCustomer(Customer customer) {
        super(customer.getEmail(), customer.getPassword(), AuthorityUtils.createAuthorityList(customer.getRole().name()));
        this.customer=customer;
    }

    public Customer getCustomer() {
        return customer;
    }
}
