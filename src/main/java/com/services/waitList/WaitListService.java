package com.services.waitList;

import com.controllers.booking.BookingRequest;
import com.controllers.waitList.WaitListRequest;
import com.models.WaitList;

import java.util.List;

public interface WaitListService {

    WaitList findOneById(Long id);
    WaitList findOneByTripIdAndCustomerId(Long tripId, Long customerId);

    List<WaitList> findAll();
    List<WaitList> findAllByTripId(Long tripId);
    List<WaitList> findAllByCustomerId(Long customerId);

    WaitList createNewWaitlist(WaitListRequest waitListRequest);
    WaitList createWaitlistFromBookingRequest(Long tripId, BookingRequest bookingRequest);

    WaitList deleteWaitlistById(Long id);



    Integer getWaitingSeatsForTripById(Long tripId);
    List<Long> getAllIdOfTripsThatHaveWaitlist();

}
