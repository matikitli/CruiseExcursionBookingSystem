package com.services.waitList;

import com.controllers.booking.BookingRequest;
import com.controllers.waitList.WaitListRequest;
import com.exceptions.notFoundExceptions.WaitListNotFoundException;
import com.models.WaitList;
import com.repositories.WaitListRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class WaitListServiceImp implements WaitListService{

    @Autowired
    private WaitListRepo waitListRepo;

    @Override
    public WaitList findOneById(Long id) {
        return waitListRepo.findOne(id);
    }

    @Override
    public WaitList findOneByTripIdAndCustomerId(Long tripId, Long customerId) {
        return waitListRepo.findAllByCustomerId(customerId).stream()
                .filter(ele->ele.getTripId().equals(tripId)).findFirst()
                .orElseThrow(() -> new WaitListNotFoundException("WaitList not found"));
    }

    @Override
    public List<WaitList> findAll() {
        return waitListRepo.findAll();
    }

    @Override
    public List<WaitList> findAllByTripId(Long tripId) {
        return waitListRepo.findAll().stream().filter(ele->ele.getTripId().equals(tripId))
                .collect(Collectors.toList());
    }

    @Override
    public List<WaitList> findAllByCustomerId(Long customerId) {
        return waitListRepo.findAll().stream().filter(ele->ele.getCustomerId().equals(customerId))
                .collect(Collectors.toList());
    }

    @Override
    public WaitList createNewWaitlist(WaitListRequest waitListRequest) {
        return waitListRepo.save(new WaitList(waitListRequest.getTripId(),
                waitListRequest.getCustomerId(),waitListRequest.getNumberOfSeats()));
    }

    @Override
    public WaitList deleteWaitlistById(Long id) {
        WaitList tmp = waitListRepo.findOne(id);
        waitListRepo.delete(id);
        return tmp;
    }

    @Override
    public Integer getWaitingSeatsForTripById(Long tripId) {
        return this.findAllByTripId(tripId).stream().mapToInt(WaitList::getNumberOfSeats).sum();
    }

    @Override
    public List<Long> getAllIdOfTripsThatHaveWaitlist() {
        return waitListRepo.findAll().stream().map(WaitList::getTripId).collect(Collectors.toList());
    }

    @Override
    public WaitList createWaitlistFromBookingRequest(Long tripId, BookingRequest bookingRequest){
        return waitListRepo.save(new WaitList(tripId,bookingRequest.getCustomerId(),
                bookingRequest.getNumberOfSeats()));
    }
}
