package com.services.booking;

import com.controllers.booking.BookingRequest;

import com.controllers.booking.BookingRequestEdit;
import com.exceptions.BookingAlreadyExistException;
import com.exceptions.notFoundExceptions.BookingNotFoundException;
import com.exceptions.notFoundExceptions.CustomerNotFoundException;
import com.exceptions.WaitlistCreatedException;
import com.models.Booking;
import com.models.Trip;
import com.models.WaitList;
import com.repositories.BookingRepo;
import com.services.customer.CustomerService;
import com.services.trip.TripService;
import com.services.waitList.WaitListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BookingServiceImp implements BookingService {

    @Autowired
    private BookingRepo bookingRepo;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private TripService tripService;

    @Autowired
    private WaitListService waitListService;

    @Override
    public Booking findBookingById(Long id) {
        return findAllBookings().stream().filter(booking->booking.getId()==id).findFirst()
                .orElseThrow(()->new BookingNotFoundException(String.format("Booking with id %d not found", id)));
    }

    @Override
    public List<Booking> findBookingsByExcursionId(Long excursionId) {
        return findAllBookings().stream().filter(booking -> booking.getTrip().getExcursion().getId().equals(excursionId))
                .collect(Collectors.toList());
    }

    @Override
    public List<Booking> findBookingsByCustomerId(Long customerId) {
        return findAllBookings().stream().filter(booking -> booking.getCustomer().getId().equals(customerId))
                .collect(Collectors.toList());
    }

    @Override
    public List<Booking> findAllBookings(){
        return bookingRepo.findAll();
    }

    @Override
    public List <Booking> findBookingByCustomerIdExcursionIdDate(Long customerId, Long excursionId, LocalDate date){
        return findAllBookings().stream()
                .filter(booking -> booking.getCustomer().getId().equals( customerId ))
                .filter(booking -> booking.getTrip().getExcursion().getId().equals( excursionId ))
                .filter(booking -> booking.getTrip().getDate().isEqual( date ))
                .collect( Collectors.toList() );
    }
    // Can we change name of this method to findAllBookingsByTripAndTripDate....???
    @Override
    public List<Booking>findAllBookingsByTripIdAndTripDate(Long tripId, LocalDate date){
        return findAllBookings().stream()
                .filter(booking -> booking.getTrip().getId().equals( tripId ))
                .filter(booking -> booking.getTrip().getDate().equals( date ))
                .collect( Collectors.toList());
    }

    @Override
    public List<Booking>findBookingsByDate(LocalDate date){
        return findAllBookings().stream()
                .filter(booking -> booking.getTrip().getDate().equals( date )).collect(Collectors.toList());
    }

    @Override
    public List<Booking>findBookingsAfterDate(LocalDate date){
        return findAllBookings().stream()
                .filter(booking -> booking.getTrip().getDate().isAfter( date ))
                .collect(Collectors.toList());
    }

    @Override
    public List<Booking>findBookingsBeforeDate(LocalDate date){
        return findAllBookings().stream()
                .filter(booking -> booking.getTrip().getDate().isBefore( date ))
                .collect(Collectors.toList());
    }

    @Override
    public List<Booking>findBookingsBetweenDates(LocalDate startDate, LocalDate endDate){
        return findAllBookings().stream()
                .filter(booking -> booking.getTrip().getDate().isAfter( startDate ))
                .filter(booking -> booking.getTrip().getDate().isBefore( endDate ))
                .collect(Collectors.toList());
    }
    @Override
    public Booking createNewBooking(BookingRequest booking) throws WaitlistCreatedException, BookingAlreadyExistException {
        if(CollectionUtils.isEmpty( findBookingByCustomerIdExcursionIdDate(booking.getCustomerId(),booking.getTrip().getExcursionId(),booking.getTrip().getDate() ) )){
            Trip ourTrip = tripService.createNewOrReturnExistingTrip(booking.getTrip());
            if(tripService.willFitOnTrip(ourTrip.getId(),booking.getNumberOfSeats())){
                return bookingRepo.save(new Booking(customerService.getById(booking.getCustomerId())
                        .orElseThrow(()->new CustomerNotFoundException("There is no customer with id: "+booking.getCustomerId())),
                        booking.getNumberOfSeats(),
                        ourTrip));
            }
            else{
                WaitList waitList = waitListService.createWaitlistFromBookingRequest(ourTrip.getId(),booking);
                //TODO: find another way to signalize it
                throw new WaitlistCreatedException(waitList.getId());
            }}
        else { throw new BookingAlreadyExistException( "This booking already exists." );}
    }

    @Override
    public Booking editBooking(BookingRequestEdit bookingRequest, Long bookingId ) {
        Booking tmp = bookingRepo.findOne(bookingId);
        tmp.setTrip(tripService.createNewOrReturnExistingTrip(bookingRequest.getTrip()));
        tmp.setNumberOfSeatsRequired(bookingRequest.getNumberOfSeats());
        return bookingRepo.save(tmp);
    }

    @Override
    public void deleteBooking(Long id) {
        bookingRepo.delete(id);
    }
}
