package com.services.booking;

import com.controllers.booking.BookingRequest;
import com.controllers.booking.BookingRequestEdit;
import com.exceptions.BookingAlreadyExistException;
import com.exceptions.WaitlistCreatedException;
import com.models.Booking;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

public interface BookingService {

    //READ
    Booking findBookingById(Long id);
    List<Booking> findBookingsByExcursionId(Long excursionId);
    List<Booking> findBookingsByCustomerId(Long customerId);
    List<Booking> findAllBookings();
    List<Booking>findBookingByCustomerIdExcursionIdDate(Long customerId, Long excursionId, LocalDate date);
    List<Booking>findAllBookingsByTripIdAndTripDate(Long tripId, LocalDate date);
    List<Booking>findBookingsByDate(LocalDate date);
    List<Booking>findBookingsAfterDate(LocalDate date);
    List<Booking>findBookingsBeforeDate(LocalDate date);
    List<Booking>findBookingsBetweenDates(LocalDate startDate, LocalDate endDate);
    //CREATE
    Booking createNewBooking(BookingRequest booking) throws WaitlistCreatedException, BookingAlreadyExistException;
    //UPDATE
    Booking editBooking(BookingRequestEdit bookingRequest, Long id );
    //DELETE
    void deleteBooking(Long id);
}
