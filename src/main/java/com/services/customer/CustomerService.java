package com.services.customer;

import com.controllers.customer.CustomerRequest;
import com.models.Customer;

import java.util.List;
import java.util.Optional;

public interface CustomerService {

    //READ
    Optional<Customer> getById(Long id);
    Optional<Customer> getByName(String name);
    Optional<Customer> getByEmail(String email);
    List<Customer> getAllCustomers();

    //CREATE
    Customer registerNewCustomer(CustomerRequest customerRequest);

    //UPDATE
    Customer editExistingCustomer(Long id,CustomerRequest customerRequest);

    //DELETE
    void deleteExistingCustomer(Long id);
}
