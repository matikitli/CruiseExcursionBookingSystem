package com.services.customer;

import com.controllers.customer.CustomerRequest;
import com.models.Customer;
import com.repositories.CustomerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Transactional
@Service
public class CustomerServiceImp implements CustomerService {

    @Autowired
    private CustomerRepo customerRepo;

    //READ
    @Override
    public Optional<Customer> getById(Long id){
        return getAllCustomers().parallelStream()
                .filter(customer -> customer.getId().equals(id))
                .findFirst();
    }

    @Override
    public Optional<Customer> getByName(String name){
        return getAllCustomers().parallelStream()
                .filter(customer -> customer.getName().equalsIgnoreCase(name))
                .findFirst();
    }

    @Override
    public Optional<Customer> getByEmail(String email){
        return getAllCustomers().parallelStream()
                .filter(customer -> customer.getEmail().equalsIgnoreCase(email))
                .findFirst();
    }

    @Override
    public List<Customer> getAllCustomers(){
        return customerRepo.findAll();
    }

    //CREATE
    @Override
    public Customer registerNewCustomer(CustomerRequest customerRequest) {
        if(!customerRepo.existsByEmail(customerRequest.getEmail())){
            return customerRepo.save(
                    new Customer(customerRequest.getName(),
                            customerRequest.getEmail(),
                            customerRequest.getPassword(),
                            customerRequest.getCabineNumber(),
                            customerRequest.getRole()));
        }
        else throw new IllegalArgumentException("Customer with given email already exists");
    }

    //UPDATE
    @Override
    public Customer editExistingCustomer(Long id, CustomerRequest customerRequestObject) {
        //check if customer with provided id exists in database
        //if is true then assign this customer to temporaryCustomer
        if (customerRepo.exists(id)) {
            if (!customerRepo.existsByEmail(customerRequestObject.getEmail())){
                Customer temporaryCustomer = customerRepo.findOne(id);

                //set new name by requesting new name which is done by creating object of class  CustomerRequest and method getName
                temporaryCustomer.setName(customerRequestObject.getName());

                //set new password by requesting new password which is done by creating object of class  CustomerRequest and method getPassword
                temporaryCustomer.setPassword(customerRequestObject.getPassword());

                //set new cabin number by requesting new cabin number which is done by creating object of class  CustomerRequest and method getCabineNumber
                temporaryCustomer.setCabineNumber(customerRequestObject.getCabineNumber());

                //set new email address and check if new email address exists in database already if yes provide appropriate message
                temporaryCustomer.setEmail(customerRequestObject.getEmail());
                return customerRepo.save(temporaryCustomer);
            }else throw new IllegalArgumentException("Customer with given email already exist");
        }
        else throw new IllegalArgumentException("Customer with given id does not exist");

    }

    //DELETE
    @Override
    public void deleteExistingCustomer(Long id) {
        customerRepo.deleteCustomerById(id);
    }
}
