package com.services.trip;

import com.controllers.trip.TripRequest;
import com.exceptions.notFoundExceptions.ExcursionNotFoundException;
import com.exceptions.notFoundExceptions.TripNotFoundException;
import com.repositories.TripRepo;
import com.services.booking.BookingService;
import com.services.excursion.ExcursionService;
import org.springframework.beans.factory.annotation.Autowired;
import com.models.Trip;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TripServiceImp implements TripService{

    private static final Integer MAX_SEATS_ON_TRIP=32;

    @Autowired
    private TripRepo tripRepo;

    @Autowired
    private ExcursionService excursionService;

    @Autowired
    private BookingService bookingService;

    //READ
    @Override
    public List<Trip> findAllTrips(){ return tripRepo.findAll();
    }

    @Override
    public List<Trip> findTripsByExcursionId(Long excursionId){
        List<Trip> tmp = findAllTrips().parallelStream()
                    .filter(trip -> trip.getExcursion().getId()
                    .equals(excursionId)).collect(Collectors.toList());
        if (tmp.isEmpty()) throw new TripNotFoundException("There are no trips for excursion: "+excursionId);
        return tmp;
    }

    @Override
    public List<Trip> findTripsByDate(LocalDate date) {
        List<Trip> tmp = findAllTrips().parallelStream()
                .filter(trip -> trip.getDate().equals(date))
                .collect(Collectors.toList());
        if (tmp.isEmpty()) throw new TripNotFoundException("There are no trips for date: "+date);
        return tmp;
    }

    @Override
    public Trip findTripById(Long id){ return findAllTrips().parallelStream()
            .filter(trip -> trip.getId().equals(id))
            .findFirst()
            .orElseThrow(()->new TripNotFoundException(String.format("Trip with id %d not found", id)));
    }

    @Override
    public Trip findTripByExcursionIdAndDate(Long excursionId, LocalDate date){
        return findAllTrips().stream()
                .filter( trip -> trip.getExcursion().getId().equals( excursionId ) )
                .filter( trip -> trip.getDate().isEqual( date) )
                .findFirst()
                .orElseThrow(()-> new TripNotFoundException( String.format( "Trip with provided information does not exist." )) );
    }

    //TODO: create more date filtering functions (like: fromDate, toDate, fromDateToDate etc)
    //LocalDate have functions isAfter(date), isBefore(date) that could be useful

    //CREATE
    @Override
    public Trip createNewOrReturnExistingTrip(TripRequest trip){
        Trip tmp = tripRepo.findByDateAndAndExcursion(trip.getDate(),excursionService.getById(trip.getExcursionId()).get());
        if(tmp!=null) {
            return tmp;
        }
        return tripRepo.save(new Trip(excursionService.getById(trip.getExcursionId()).
                        orElseThrow(()->new ExcursionNotFoundException("There is no excursion with id: "+trip.getExcursionId())),
                        trip.getDate()));
    }


    //UPDATE
    @Override
    public Trip editTrip(Trip newTrip){return tripRepo.save(newTrip);}

    //DELETE
    @Override
    public void deleteTrip(Long id){ tripRepo.delete(id);}


    //OTHERS
    @Override
    public Integer getFreeSeatsByTripId(Long tripId) {
        return MAX_SEATS_ON_TRIP - bookingService.findAllBookings().stream()
                .filter(ele->ele.getTrip().getId().equals(tripId))
                .mapToInt(ele->ele.getNumberOfSeatsRequired()).sum();
    }

    @Override
    public boolean willFitOnTrip(Long tripId, int seats) {
        return this.getFreeSeatsByTripId(tripId) >= seats;
    }

}
