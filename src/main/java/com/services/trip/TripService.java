package com.services.trip;

import com.controllers.trip.TripRequest;
import com.models.Trip;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;



public interface TripService {

    //READ
    Trip findTripById(Long id);
    List<Trip> findTripsByExcursionId(Long excursionId);
    List<Trip> findTripsByDate(LocalDate date);
    List<Trip> findAllTrips();
    Trip findTripByExcursionIdAndDate(Long excursionId, LocalDate date);



    //CREATE
    Trip createNewOrReturnExistingTrip(TripRequest trip);

    //UPDATE
    Trip editTrip(Trip newTrip);

    //DELETE
    void deleteTrip(Long id);

    //OTHERS
    Integer getFreeSeatsByTripId(Long id);
    boolean willFitOnTrip(Long tripId, int seats);

}
