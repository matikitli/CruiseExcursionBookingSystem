package com;

import com.controllers.customer.CustomerRequest;
import com.models.Booking;
import com.models.Trip;
import com.models.WaitList;
import com.repositories.TripRepo;
import com.models.Excursion;
import com.opencsv.CSVReader;
import com.repositories.BookingRepo;
import com.repositories.ExcursionRepo;
import com.repositories.WaitListRepo;
import com.security.Role;
import com.services.customer.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.io.FileReader;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class StartupDatabaseCreator implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private ExcursionRepo excursionRepo;

    @Autowired
    private BookingRepo bookingRepo;

    @Autowired
    private TripRepo tripRepo;

    @Autowired
    private WaitListRepo waitListRepo;

    private static String FILE = "/excursion.csv";
    private CSVReader reader = null;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event){

        if(!areDatabaseEmpty()){
            return;
        }

            //EXCURSIONS
        try {
            createTableExcursions();
        } catch (Exception e) {
            e.printStackTrace();
        }
            //CUSTOMERS
            createTableCustomers();
            //TRIPS
            createTableTrips();
            //BOOKINGS
            createTableBookings();
            //WAITLIST
            createTableWaitlist();


        /*Map<Long, Integer> mapExcIdAndSeats=bookingRepo.findAll().stream()
                    .collect(Collectors.groupingBy(booking->booking.getTrip().getId())).entrySet().stream()
                    .collect(Collectors.toMap(ee->ee.getKey(),
                                                e->e.getValue().stream()
                    .map(el->el.getNumberOfSeatsRequired()).reduce(0,(a,b)->a+b)));

        System.out.println(mapExcIdAndSeats);*/

        System.out.println("DATABASE CREATED");



    }
    private void createTableTrips() {

        Trip trip1 = new Trip();
        trip1.setExcursion(excursionRepo.findAll().get(0));
        trip1.setDate(LocalDate.now());

        Trip trip2 = new Trip();
        trip2.setExcursion(excursionRepo.findAll().get(1));
        trip2.setDate(LocalDate.now().plusDays(10));

        Trip trip3 = new Trip();
        trip3.setExcursion(excursionRepo.findAll().get(2));
        trip3.setDate(LocalDate.now().plusDays(15));

        Trip trip4 = new Trip();
        trip4.setExcursion(excursionRepo.findAll().get(1));
        trip4.setDate(LocalDate.now().plusDays(20));

        tripRepo.save(Arrays.asList(trip1,trip2,trip3,trip4));

    }

    private void createTableBookings() {

        Booking booking1 = new Booking(4);
        booking1.setCustomer(customerService.getAllCustomers().get(0));
        booking1.setTrip(tripRepo.findAll().get(0));

        Booking booking2 = new Booking(3);
        booking2.setCustomer(customerService.getAllCustomers().get(1));
        booking2.setTrip(tripRepo.findAll().get(1));

        Booking booking3 = new Booking(7);
        booking3.setCustomer(customerService.getAllCustomers().get(2));
        booking3.setTrip(tripRepo.findAll().get(2));

        Booking booking4 = new Booking(3);
        booking4.setCustomer(customerService.getAllCustomers().get(1));
        booking4.setTrip(tripRepo.findAll().get(3));

        bookingRepo.save(Arrays.asList(booking1,booking2,booking3,booking4));
    }


    private boolean areDatabaseEmpty(){
        return customerService.getAllCustomers().size()==0 &&
                excursionRepo.findAll().size()==0 &&
                tripRepo.findAll().size()==0 &&
                bookingRepo.findAll().size()==0;
    }

    private void createTableExcursions() throws Exception{
        reader = new CSVReader(new FileReader(getClass().getResource(FILE).getPath()), ',', '\'',1);
        String[] line;
        List<Excursion> list = new ArrayList<>();
        while((line = reader.readNext()) != null) {
            list.add(new Excursion(Long.parseLong(line[0]), line[1], line[2]));
        }
        excursionRepo.save(list);
    }

    private void createTableCustomers(){
        customerService.registerNewCustomer(new CustomerRequest("Mateusz","aa@bb.cc","1234",1, Role.USER));
        customerService.registerNewCustomer(new CustomerRequest("Filip","dd@ee.ff","1234",2,Role.USER));
        customerService.registerNewCustomer(new CustomerRequest("Sandra","gg@hh.ii","1234",3,Role.USER));
        customerService.registerNewCustomer(new CustomerRequest("Arek","jj@kk.ll","1234",3,Role.ADMIN));
        customerService.registerNewCustomer(new CustomerRequest("Swirek","mm@nn.oo","1234",9,Role.USER));


    }

    private void createTableWaitlist(){
        WaitList waitList1 = new WaitList(tripRepo.findAll().get(0).getId(),
                customerService.getByEmail("dd@ee.ff").get().getId(),3);
        WaitList waitList2 = new WaitList(tripRepo.findAll().get(1).getId(),
                customerService.getByEmail("mm@nn.oo").get().getId(),5);
        WaitList waitList3 = new WaitList(tripRepo.findAll().get(2).getId(),
                customerService.getByEmail("jj@kk.ll").get().getId(),2);
        waitListRepo.save(Arrays.asList(waitList1,waitList2,waitList3));
    }
}
