package com.models;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Entity
@Table(name = "wait_lists",
        uniqueConstraints = {@UniqueConstraint( columnNames = {"customer_id","trip_id"})})
public class WaitList {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "trip_id", unique = true)
    private Long tripId;

    @Column(name = "customer_id")
    private Long customerId;

    @Min(1) @Max(10)
    private int numberOfSeats;

    public WaitList(){}
    public WaitList(Long tripId, Long customerId, int numberOfSeats) {
        this.tripId = tripId;
        this.customerId = customerId;
        this.numberOfSeats = numberOfSeats;
    }

    public Long getId() {
        return id;
    }

    public Long getTripId() {
        return tripId;
    }

    public void setTripId(Long tripId) {
        this.tripId = tripId;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    public void setNumberOfSeats(int numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }
}
