package com.models;

import javax.persistence.*;
import java.time.LocalDate;


@Entity
@Table(name = "trips",
        uniqueConstraints = {@UniqueConstraint( columnNames = {"excursion_id", "date"})})
public class Trip {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "excursion_id",nullable = false)
    private Excursion excursion;

    private LocalDate date;

    public Trip() {}

    public Trip(Excursion excursion, LocalDate date) {
        this.excursion = excursion;
        this.date = date;
    }

    public Long getId() {
        return id;
    }

    public Excursion getExcursion() {
        return excursion;
    }

    public void setExcursion(Excursion excursion) {
        this.excursion = excursion;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Trip{" +
                "id=" + id +
                ", excursion=" + excursion +
                ", date=" + date +
                '}';
    }


}
