package com.models;

import com.security.Role;

import javax.persistence.*;

@Entity
@Table(name = "Customers")
public class Customer {
    /*
    |----------------------------------------------------------------------|
    |IN CASE OF SWITCHING TO DAO  (in future mb)                           |
    |----------------------------------------------------------------------|
    |hierarchy: controller > service > dao > persistence & repo            |
    |return:   (Customer)|(Customer)|(Customer)| persistence               |
    |IMPORTANT: this will become CustomerPersistence                       |
    |Customer is domain object                                             |
    |CustomerDao is object to communicate with db. Data access object      |
    |CustomerPersistence(entity) is object in db                           |
    |----------------------------------------------------------------------|
    */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "Name")
    private String name;

    @Column(name = "Email",unique = true)
    private String email;

    @Column(name = "CabineNumber")
    private int cabineNumber;

    @Column(name = "Password")
    private String password;

    @Column(name = "Role")
    @Enumerated(EnumType.STRING)
    private Role role=Role.USER;

    public Customer(){}

    public Customer(String name, String email, String password,int cabineNumber,Role role) {
        this.name = name;
        this.email = email;
        this.password=password;
        this.cabineNumber = cabineNumber;
        this.role=role;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getCabineNumber() {
        return cabineNumber;
    }

    public void setCabineNumber(int cabineNumber) {
        this.cabineNumber = cabineNumber;
    }

    public Role getRole() { return role; }

    public void setRole(Role role) { this.role = role; }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", cabineNumber=" + cabineNumber +
                ", password='" + password + '\'' +
                ", role=" + role +
                '}';
    }
}
