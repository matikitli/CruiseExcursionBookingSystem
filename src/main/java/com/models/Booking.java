package com.models;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.io.Serializable;

@Entity
@Table(name = "bookings",
        uniqueConstraints = {@UniqueConstraint( columnNames = {"customer_id","trip_id"})})
public class Booking {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "customer_id",nullable = false)
    private Customer customer;

    @Min(0) @Max(10)
    private int numberOfSeatsRequired;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "trip_id")
    private Trip trip;


    public Booking() {}

    public Booking(int numberOfSeatsRequired) {
        this.numberOfSeatsRequired = numberOfSeatsRequired;
    }

    public Booking(Customer customer, int numberOfSeatsRequired, Trip trip) {
        this.customer = customer;
        this.numberOfSeatsRequired = numberOfSeatsRequired;
        this.trip = trip;
    }

    public long getId() {
        return id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public Booking setCustomer(Customer customer) {
        this.customer = customer;
        return this;
    }

    public int getNumberOfSeatsRequired() {
        return numberOfSeatsRequired;
    }

    public void setNumberOfSeatsRequired(int numberOfSeatsRequired) {
        this.numberOfSeatsRequired = numberOfSeatsRequired;
    }

    public Trip getTrip() {
        return trip;
    }

    public Booking setTrip(Trip trip) {
        this.trip = trip;
        return this;
    }


    @Override
    public String toString() {
        return "Booking{" +
                "id=" + id +
                ", customer=" + customer +
                ", numberOfSeatsRequired=" + numberOfSeatsRequired +
                ", trip=" + trip +
                '}';
    }
}
