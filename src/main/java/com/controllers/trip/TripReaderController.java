package com.controllers.trip;

import com.services.trip.TripService;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@Api(value = "Trip Reader", description = "(GET)")
@RequestMapping(value = "/api/trips")

public class TripReaderController {

    @Autowired
    private TripService tripService;

    @GetMapping("/findAll")
    @ResponseBody
    List<TripResponse> findAllTrips(){
        return tripService.findAllTrips().stream().
                map(trip -> new TripResponse(trip)).collect(Collectors.toList());
    }

    @GetMapping("/findOneById")
    @ResponseBody
    public TripResponse findTripById(@RequestParam(name = "id", required = true)Long id){
        return new TripResponse(tripService.findTripById(id));
    }

    @GetMapping("/findAllByExcursionId")
    @ResponseBody
    List<TripResponse> findTripsByExcursionId(@RequestParam(name = "id", required = true)Long id){
        return tripService.findTripsByExcursionId(id).stream().
                map(trip -> new TripResponse(trip)).collect(Collectors.toList());
    }

    @GetMapping("/findAllByDate")
    @ResponseBody
    List<TripResponse> findTripsByDate(@RequestParam(name = "date", required = true)
                                               @DateTimeFormat(iso = DateTimeFormat.ISO.DATE,pattern = "dd-MM-yyyy")
                                               LocalDate date){
       return tripService.findTripsByDate(date).stream()
               .map(trip -> new TripResponse(trip)).collect(Collectors.toList());
    }

    @GetMapping("/findOneByExcursionIdAndDate")
    @ResponseBody
    public TripResponse findTripByExcursionIdAndDate(@RequestParam(name = "excursionId", required = true)Long excursionId,
                                                     @RequestParam(name = "date", required = true)@DateTimeFormat(iso = DateTimeFormat.ISO.DATE,pattern = "dd-MM-yyyy") LocalDate date){
        return new TripResponse( tripService.findTripByExcursionIdAndDate( excursionId, date ) );
    }


}
