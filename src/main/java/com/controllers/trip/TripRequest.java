package com.controllers.trip;

import com.configurations.dateTimeConverters.LocalDateDeserializer;
import com.configurations.dateTimeConverters.LocalDateSerializer;
import com.controllers.excursion.ExcursionResponse;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Size;
import java.time.LocalDate;

@ApiModel("TripRequest")
public class TripRequest {

    @ApiModelProperty(value = "Excursion Id", required = true, example = "12345")
    @Size(min = 5, max = 7)
    private Long excursionId;

    @ApiModelProperty(value = "Date", required = true, example = "24-04-2018")
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private LocalDate date;

    @JsonCreator
    public TripRequest(@JsonProperty("excursionId") Long excursionId,
                       @JsonProperty("date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE,pattern = "dd-MM-yyyy") LocalDate date) {
        this.excursionId = excursionId;
        this.date = date;
    }


    public Long getExcursionId() {
        return excursionId;
    }

    public LocalDate getDate() {
        return date;
    }
}
