package com.controllers.trip;


import com.services.trip.TripService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@Api(value = "Trip Writer Controller", description ="(POST, PUT, DELETE)")
@RequestMapping("/api/trips")

public class TripWriterController {

    @Autowired
    private TripService tripService;

    @PostMapping("/create")
    @ResponseBody
    //example body: {"excursionId":"970044","date":"2018-04-04"}
    TripResponse createNewTrip(@Valid @RequestBody TripRequest tripRequest)throws IllegalArgumentException{
        return new TripResponse( tripService.createNewOrReturnExistingTrip(tripRequest));
    }


    @DeleteMapping("/delete")
    @ResponseBody
    String deleteTrip(@RequestParam("id") Long id){
        tripService.deleteTrip( id );
        return "Trip deleted";
    }

}
