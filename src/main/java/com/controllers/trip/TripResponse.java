package com.controllers.trip;

import com.configurations.dateTimeConverters.LocalDateDeserializer;
import com.configurations.dateTimeConverters.LocalDateSerializer;
import com.controllers.excursion.ExcursionResponse;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.models.Trip;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@ApiModel(value = "TripResponse")

public class TripResponse {

    @ApiModelProperty(value = "Trip's ID in database", required = true, example = "123")
    private Long id;

    @ApiModelProperty(value = "Excursion response", required = true, example = "...")
    private ExcursionResponse excursionResponse;

    @ApiModelProperty(value = "Date of booking", required = true, example = "22-03-2016")
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private LocalDate date;

    @JsonCreator
    public TripResponse(@JsonProperty("id")Long id,
                        @JsonProperty("excursion")ExcursionResponse excursionResponse,
                        @JsonProperty("date") LocalDate date){
        this.id = id;
        this.excursionResponse = excursionResponse;
        this.date = date;

    }

    public TripResponse (Trip tripToMap){
        this(tripToMap.getId(),new ExcursionResponse(tripToMap.getExcursion()),tripToMap.getDate());
    }

    public TripResponse() {
    }

    public Long getId() {
        return id;
    }

    public ExcursionResponse getExcursion() {
        return excursionResponse;
    }

    public LocalDate getDate() {
        return date;
    }
}
