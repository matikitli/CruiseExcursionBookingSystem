package com.controllers.booking;

import com.exceptions.PermissionDeniedException;
import com.security.CurrentCustomerSecurityDetailsService;
import com.services.booking.BookingService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@Api(value = "Booking Reader Controller", description = "(GET)")
@RequestMapping("/api/bookings")
public class BookingReaderController {

    @Autowired
    private BookingService bookingService;

    @Autowired
    private CurrentCustomerSecurityDetailsService detailsService;

    @GetMapping("/findAll")
    @ResponseBody
    List<BookingResponse> findAllBookings(){
        return bookingService.findAllBookings().stream().
                map(booking -> new BookingResponse(booking)).collect(Collectors.toList());
    }

    @GetMapping("/findOneById")
    @ResponseBody
    public BookingResponse findBookingById(@RequestParam(name = "id", required = true)Long id) throws Exception {
        if(detailsService.canAccess("booking", id))
            return new BookingResponse(bookingService.findBookingById(id));
        else throw new PermissionDeniedException("You can only find bookings that are assigned to your id.");
    }

    @GetMapping("/findAllByExcursionId")
    @ResponseBody
    List<BookingResponse> findBookingsByExcursionId(@RequestParam(name = "id", required = true)Long id){
        return bookingService.findBookingsByExcursionId(id).stream().map(booking -> new BookingResponse(booking)).collect(Collectors.toList());
    }

    @GetMapping("/findAllByCustomerId")
    @ResponseBody
    List<BookingResponse> findBookingsByCustomerId(@RequestParam(name = "id", required = true)Long id) throws Exception {
        if (detailsService.canAccess("customer", id)) {
            return bookingService.findBookingsByCustomerId(id).stream().map(booking -> new BookingResponse(booking)).collect(Collectors.toList());
        }
        throw new PermissionDeniedException("You can only find bookings that are assigned to your id.");
    }

    @GetMapping("/findOneByCustomerIdExcursionIdDate")
    @ResponseBody
    public List<BookingResponse>findBookingByCustomerIdExcursionIdDate(@RequestParam(name = "customerId", required = true) Long customerId,
                                                                  @RequestParam(name = "excursionId", required = true)Long excursionId,
                                                                  @RequestParam(name = "date", required = true)@DateTimeFormat(iso = DateTimeFormat.ISO.DATE,pattern = "dd-MM-yyyy") LocalDate date) throws Exception {
        if (detailsService.canAccess("customer", customerId)) {
            return bookingService.findBookingByCustomerIdExcursionIdDate( customerId, excursionId, date ).stream().map(booking -> new BookingResponse( booking )).collect( Collectors.toList() );
        }
        throw new PermissionDeniedException("You can only find bookings that are assigned to your id.");
    }

    @GetMapping("/findAllByDateTrip")
    @ResponseBody
    List<BookingResponse> findAllBookingsByDateTrip (@RequestParam(name = "tripId", required = true) Long tripId,
                                                     @RequestParam(name = "date", required = true) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE,pattern = "dd-MM-yyyy")
                                                             LocalDate date){
        return bookingService.findAllBookingsByTripIdAndTripDate(tripId , date).stream().map(booking -> new BookingResponse( booking) ).collect(Collectors.toList());
    }




}
