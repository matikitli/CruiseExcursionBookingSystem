package com.controllers.booking;

import com.exceptions.BookingAlreadyExistException;
import com.exceptions.PermissionDeniedException;
import com.exceptions.WaitlistCreatedException;
import com.security.CurrentCustomerSecurityDetailsService;
import com.services.booking.BookingService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@Api(value = "Booking Writer Controller", description ="(POST, PUT, DELETE)")
@RequestMapping("/api/bookings")
public class BookingWriterController {

    @Autowired
    private BookingService bookingService;

    @Autowired
    private CurrentCustomerSecurityDetailsService detailsService;

    @PostMapping("/create")
    @ResponseBody
    //example body: {"customerId":2,"seats":5,"trip":{"excursionId":"970044","date":"2018-04-04"}}
    BookingResponse createNewBooking(@Valid @RequestBody BookingRequest bookingRequest) throws WaitlistCreatedException, BookingAlreadyExistException {
        return new BookingResponse(bookingService.createNewBooking(bookingRequest));
    }

    @PutMapping("/update")
    @ResponseBody
    BookingResponse updateBooking(@RequestParam("id") Long id, @Valid @RequestBody BookingRequestEdit bookingRequest ) throws Exception {
        if (detailsService.canAccess("booking", id)) {
            return new BookingResponse(bookingService.editBooking(bookingRequest, id));
        }
        throw new PermissionDeniedException("You can only edit booking assigned to your customer id.");

    }

    @DeleteMapping("/delete")
    @ResponseBody
    String deleteBooking(@RequestParam("id") Long id) throws Exception {
        if(detailsService.canAccess("booking", id)){
            bookingService.deleteBooking(id);
            return "Booking deleted";
        }
        throw new PermissionDeniedException("You can only delete booking assigned to your customer id.");
    }

}


