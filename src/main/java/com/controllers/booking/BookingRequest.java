package com.controllers.booking;

import com.controllers.trip.TripRequest;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "BookingRequest")
public class BookingRequest {

    @ApiModelProperty(value = "Customer Id", required = true, example = "1")
    private Long customerId;

    @ApiModelProperty(value = "Number of seats required", required = true, example = "4")
    private int numberOfSeats;

    @ApiModelProperty(value = "Trip request", required = true, example = "...")
    private TripRequest trip;

    @JsonCreator
    public BookingRequest(
            @JsonProperty("customerId") Long customerId,
            @JsonProperty("seats") int numberOfSeats,
            @JsonProperty("trip") TripRequest trip) {
        this.customerId = customerId;
        this.numberOfSeats = numberOfSeats;
        this.trip = trip;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    public TripRequest getTrip() {
        return trip;
    }
}
