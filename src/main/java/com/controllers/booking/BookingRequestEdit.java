package com.controllers.booking;

import com.controllers.trip.TripRequest;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "BookingRequestEdit")
public class BookingRequestEdit {

    @ApiModelProperty(value = "Number of seats required", required = true, example = "4")
    private int numberOfSeats;

    @ApiModelProperty(value = "Trip request", required = true, example = "...")
    private TripRequest trip;

    @JsonCreator
    public BookingRequestEdit(
            @JsonProperty("seats") int numberOfSeats,
            @JsonProperty("trip") TripRequest trip) {
        this.numberOfSeats = numberOfSeats;
        this.trip = trip;
    }


    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    public TripRequest getTrip() {
        return trip;
    }
}
