package com.controllers.booking;

import com.controllers.trip.TripResponse;
import com.controllers.customer.CustomerResponse;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.models.Booking;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "BookingResponse")
public class BookingResponse {

    @ApiModelProperty(value = "Booking id", required = true, example = "3")
    private Long id;

    @ApiModelProperty(value = "Customer response", required = true, example = "...")
    private CustomerResponse customerResponse;

    @ApiModelProperty(value = "Excursion response",required = true,example = "...")
    private TripResponse tripResponse;

    //@ApiModelProperty(value = "Number of seats require", required = true, example = "44")
    private int numberOfSeats;

    @JsonCreator
    public BookingResponse(@JsonProperty("id") Long id,
                           @JsonProperty("customer") CustomerResponse customerResponse,
                           @JsonProperty("trip") TripResponse tripResponse,
                           @JsonProperty("seats") int numberOfSeats) {
        this.id = id;
        this.customerResponse = customerResponse;
        this.tripResponse = tripResponse;
        this.numberOfSeats=numberOfSeats;

    }

    public BookingResponse(Booking booking){
        this(booking.getId(),
                new CustomerResponse(booking.getCustomer()),
                new TripResponse (booking.getTrip()),
                booking.getNumberOfSeatsRequired());
    }

    public Long getId() {
        return id;
    }

    public CustomerResponse getCustomer() {
        return customerResponse;
    }

    public TripResponse getTrips() {
        return tripResponse;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }
}
