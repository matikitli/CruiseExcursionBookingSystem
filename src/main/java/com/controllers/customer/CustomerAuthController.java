package com.controllers.customer;

import com.exceptions.notFoundExceptions.CustomerNotFoundException;
import com.security.CurrentCustomerSecurityDetailsService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@Api(value = "Customer Authentication", description = "")
@RequestMapping("/api/customers")
public class CustomerAuthController {

    @Autowired
    private CurrentCustomerSecurityDetailsService currentCustomerSecurityDetailsService;

        @PostMapping("/loginPerformed")
    @ResponseBody
    public CustomerResponse loginPerformedSuccessResponse() throws Exception {
        return this.currentCustomer();
    }

    @GetMapping("/logoutPerformed")
    @ResponseBody
    public String logoutPerformedSuccessResponse(){
        currentCustomerSecurityDetailsService.logoutPerformed();
        return "Logout successful";
    }

    @GetMapping(value = "/current")
    @ResponseBody
    public CustomerResponse currentCustomer() throws Exception {
        return new CustomerResponse(currentCustomerSecurityDetailsService.getCurrentCustomer().getCustomer());
    }
}
