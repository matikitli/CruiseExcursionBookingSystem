package com.controllers.customer;

import com.exceptions.PermissionDeniedException;
import com.models.Customer;
import com.security.CurrentCustomerSecurityDetailsService;
import com.services.customer.CustomerService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController //its saying to our spring that this whole class is controller
/*
RequestMapping -> saying to spring that this will be some endpoint in our app
    value -> specifying the endpoint for whole class
    method -> specifying that this class/controller will be responsible for GET only
 */
@Api(value = "Customer Reader", description = "(GET)")
@RequestMapping(value = "/api/customers",method = RequestMethod.GET)
public class CustomerReaderController {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private CurrentCustomerSecurityDetailsService detailsService;

    @RequestMapping(value = "/findOne")
    @ResponseBody
    public CustomerResponse findOneCustomer(@RequestParam(name = "id",required = false) Long id,
                                    @RequestParam(name = "name",required = false) String name,
                                    @RequestParam(name = "email",required = false) String email) throws Exception {
        Customer customer = customerService.getById(id)
                .orElseGet(()->customerService.getByName(name)
                        .orElseGet(()->customerService.getByEmail(email).get()));
        if(detailsService.canAccess("customer", customer.getId())) {
            return new CustomerResponse(customer);
        }
        throw new PermissionDeniedException("You can only find your own profile.");
    }

    @RequestMapping(value = "/findAll")
    @ResponseBody
    List<CustomerResponse> findAllCustomers(){
        return customerService.getAllCustomers().stream().
                map(cust -> new CustomerResponse(cust)).collect(Collectors.toList());
    }


}
