package com.controllers.customer;


import com.exceptions.PermissionDeniedException;
import com.security.CurrentCustomerSecurityDetailsService;
import com.services.customer.CustomerService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@Api(value = "Customer Writer",description = "(POST, PUT, DELETE)")
@RequestMapping(value = "/api/customers")
public class CustomerWriterController {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private CurrentCustomerSecurityDetailsService detailsService;

    @PostMapping("/register")
    @ResponseBody
    CustomerResponse registerNewCustomer(@Valid @RequestBody CustomerRequest customerRequest) throws IllegalArgumentException{
        return new CustomerResponse(customerService.registerNewCustomer(customerRequest));
    }


    @PutMapping("/update")
    @ResponseBody
    CustomerResponse updateCurrentCustomer(@RequestParam ("id")Long id,@Valid @RequestBody CustomerRequest customerRequest) throws Exception {
        if(detailsService.canAccess("customer", id)){
            return new CustomerResponse(customerService.editExistingCustomer(id,customerRequest));
        }
        throw new PermissionDeniedException("You can only update your own profile");
    }

    @DeleteMapping("/delete")
    @ResponseBody
    String deleteCustomer(@RequestParam("id") Long id) throws Exception {
        if(detailsService.canAccess("customer", id)){
            customerService.deleteExistingCustomer(id);
            return "customer deleted.";
        }
        throw new PermissionDeniedException("You can only delete your own profile");
    }

}
