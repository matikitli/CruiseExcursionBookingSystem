package com.controllers.customer;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.security.Role;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Email;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@ApiModel(value = "CustomerRequest")
public class CustomerRequest {

    @ApiModelProperty(value = "Name",required = true, example = "Steven")
    @NotNull
    @Size(min = 3, max = 10)
    private String name;

    @ApiModelProperty(value = "Email", required = true, example = "abcd@efg.hj")
    @NotNull
    @Email
    @Size(min = 5, max = 30)
    private String email;

    @ApiModelProperty(value = "Password", required = true, example = "abcd1234")
    @NotNull
    @Size(min = 3, max = 30)
    private String password;

    @ApiModelProperty(value = "Cabine Number", required = true, example = "7")
    @NotNull
    @Min(1) @Max(30)
    private int cabineNumber;

    @ApiModelProperty(value = "Role", required = false, example = "USER")
    @Enumerated(EnumType.STRING)
    private Role role;

    @JsonCreator
    public CustomerRequest(@JsonProperty("name") String name,
                           @JsonProperty("email") String email,
                           @JsonProperty("password") String password,
                           @JsonProperty("cabineNumber") int cabineNumber,
                           @JsonProperty("role") Role role) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.cabineNumber = cabineNumber;
        this.role=role;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public int getCabineNumber() {
        return cabineNumber;
    }

    public Role getRole() {
        return role;
    }
}
