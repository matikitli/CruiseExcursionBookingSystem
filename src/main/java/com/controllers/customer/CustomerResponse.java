package com.controllers.customer;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.models.Customer;
import com.security.Role;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@ApiModel(value = "CustomerResponse")
public class CustomerResponse {

    @ApiModelProperty(value = "Customer's ID in database", required = true, example = "3")
    private Long id;

    @ApiModelProperty(value = "Customer's name",required = true, example = "Steven")
    private String name;

    @ApiModelProperty(value = "Customer's email", required = true, example = "random@random.pl")
    private String email;

    @ApiModelProperty(value = "Customer's cabine number", required = true, example = "10")
    private int cabineNumber;

    @ApiModelProperty(value = "Role", required = true, example = "ADMIN")
    @Enumerated(EnumType.STRING)
    private Role role;


    @JsonCreator
    public CustomerResponse(@JsonProperty("id") Long id,
                            @JsonProperty("name") String name,
                            @JsonProperty("email") String email,
                            @JsonProperty("cabineNumber") int cabineNumber,
                            @JsonProperty("role") Role role) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.cabineNumber = cabineNumber;
        this.role=role;
    }

    public CustomerResponse(Customer customerToMap){
        this(customerToMap.getId(), customerToMap.getName(),customerToMap.getEmail(),customerToMap.getCabineNumber(),customerToMap.getRole());
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }


    public String getEmail() {
        return email;
    }

    public int getCabineNumber() {
        return cabineNumber;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
