package com.controllers.excursion;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.models.Excursion;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "ExcursionResponse")
public class ExcursionResponse {

    @ApiModelProperty(value = "Exursion's ID in database", required = true, example = "123")
    private Long id;

    @ApiModelProperty(value = "Exursion's port ID in database", required = true, example = "123456")
    private String port_id;

    @ApiModelProperty(value = "Exursion's name in database", required = true, example = "Explore Rhodes")
    private String name;

    @JsonCreator
    public ExcursionResponse(@JsonProperty("id")Long id,
                            @JsonProperty("port_id")String port_id,
                            @JsonProperty("name")String name ){
        this.id = id;
        this.port_id = port_id;
        this.name = name;

    }

    public ExcursionResponse (Excursion excursionToMap){
        this(excursionToMap.getId(),excursionToMap.getPort_id(),excursionToMap.getName());
    }

    public ExcursionResponse() {
    }

    public Long getId(){return id;}

    public String getPort_id(){return port_id;}

    public String getName(){return name;}

}



