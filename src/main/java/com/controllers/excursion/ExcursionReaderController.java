package com.controllers.excursion;


import com.services.excursion.ExcursionService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@Api(value = "Excursion Reader",description = "(GET)")
@RequestMapping(value = "/api/excursions",method = RequestMethod.GET)
public class ExcursionReaderController {

    @Autowired
    private ExcursionService excursionService;

    @RequestMapping(value = "/findOne")
    @ResponseBody
    public ExcursionResponse findExcursion(@RequestParam(name = "id",required = false) Long id,
                                   @RequestParam(name="port_id",required = false) String port_id) {

        return new ExcursionResponse (excursionService.getById(id)
                .orElseGet(() ->excursionService.getByPortId(port_id).get()));
    }

    @RequestMapping(value = "/findAll")
    @ResponseBody
    public List<ExcursionResponse> findExcursionsWithGivenWord(@RequestParam(name = "word",defaultValue = "allexc") String word){
        return excursionService.getAllExcursionsByWord(word).stream().map(excursion -> new ExcursionResponse(excursion) ).collect(Collectors.toList());
    }





}
