package com.controllers.waitList;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.models.WaitList;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

@ApiModel("WaitListRequest")
public class WaitListRequest {

    @ApiModelProperty(value = "Trip Id",required = true, example = "1234")
    @Size(min=1)
    private Long tripId;

    @ApiModelProperty(value = "Customer Id", required = true, example = "1234")
    @Size(min = 1)
    private Long customerId;

    @ApiModelProperty(value = "Number of seats", required = true, example = "4")
    @Min(1) @Max(10)
    private int numberOfSeats;

    @JsonCreator
    public WaitListRequest(@JsonProperty("tripId") Long tripId,
                           @JsonProperty("customerId") Long customerId,
                           @JsonProperty("seats") int numberOfSeats) {
        this.tripId = tripId;
        this.customerId = customerId;
        this.numberOfSeats = numberOfSeats;
    }

    public Long getTripId() {
        return tripId;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }
}
