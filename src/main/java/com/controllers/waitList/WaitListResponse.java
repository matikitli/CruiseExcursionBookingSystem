package com.controllers.waitList;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.models.WaitList;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("WaitListResponse")
public class WaitListResponse {

    @ApiModelProperty(value = "WaitList Id",required = true,example = "1234")
    private Long id;

    @ApiModelProperty(value = "Trip Id", required = true, example = "1234")
    private Long tripId;

    @ApiModelProperty(value = "Customer Id", required = true, example = "1234")
    private Long customerId;

    @ApiModelProperty(value = "Number of seats", required = true, example = "4")
    private int numberOfSeats;

    @JsonCreator
    public WaitListResponse(@JsonProperty("id") Long id,
                            @JsonProperty("tripId") Long tripId,
                            @JsonProperty("customerId") Long customerId,
                            @JsonProperty("seats") int numberOfSeats) {
        this.id = id;
        this.tripId = tripId;
        this.customerId = customerId;
        this.numberOfSeats = numberOfSeats;
    }

    public WaitListResponse(WaitList waitList){
        this(waitList.getId(),waitList.getTripId(),waitList.getCustomerId(),waitList.getNumberOfSeats());
    }

    public Long getId() {
        return id;
    }

    public Long getTripId() {
        return tripId;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }
}
