package com.controllers.waitList;

import com.exceptions.PermissionDeniedException;
import com.security.CurrentCustomerSecurityDetailsService;
import com.services.waitList.WaitListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api/waitlists")
public class WaitListReaderController {

    @Autowired
    private WaitListService waitListService;

    @Autowired
    private CurrentCustomerSecurityDetailsService detailsService;

    @GetMapping(value = "/findAll")
    @ResponseBody
    List<WaitListResponse> findAll(){
        return waitListService.findAll().stream().map(WaitListResponse::new)
                .collect(Collectors.toList());
    }

    @GetMapping(value = "/findAllByTripId")
    @ResponseBody
    List<WaitListResponse> findAllByTripId(@RequestParam(value = "tripId") Long id){
        return waitListService.findAllByTripId(id).stream().map(WaitListResponse::new)
                .collect(Collectors.toList());
    }

    @GetMapping(value = "/findAllByCustomerId")
    List<WaitListResponse> findAllByCustomerId(@RequestParam(value = "customerId") Long id) throws Exception {
        if(detailsService.canAccess("customer", id)) {
            return waitListService.findAllByCustomerId(id).stream().map(WaitListResponse::new)
                    .collect(Collectors.toList());
        }
        throw new PermissionDeniedException("You can only find waitlist assigned to your customer id.");
    }

    @GetMapping(value = "/findOneById")
    @ResponseBody
    WaitListResponse findOneById(@RequestParam(value = "id") Long id) throws Exception {
        if(detailsService.canAccess("waitlist", id)){
            return new WaitListResponse(waitListService.findOneById(id));
        }
        throw new PermissionDeniedException("You can only find waitlist assigned to your customer id.");
    }

    @GetMapping(value = "/findOneByTripIdCustomerId")
    @ResponseBody
    WaitListResponse findOneByTripIdAndCustomerId(@RequestParam(value = "tripId") Long tripId,
                                                  @RequestParam(value = "customerId") Long customerId) throws Exception {
        if(detailsService.canAccess("customer", customerId)) {
            return new WaitListResponse(waitListService.findOneByTripIdAndCustomerId(tripId, customerId));
        }
        throw new PermissionDeniedException("You can only find waitlist assigned to your customer id.");
    }


}
