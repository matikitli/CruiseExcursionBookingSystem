package com.repositories;
import com.models.Excursion;
import com.models.Trip;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;

public interface TripRepo extends JpaRepository<Trip,Long> {

    //READ
    Trip findByDateAndAndExcursion(LocalDate date, Excursion excursion);
    //CREATE

    //UPDATE

    //DELETE

}
