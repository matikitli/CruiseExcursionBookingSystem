package com.repositories;

import com.models.WaitList;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface WaitListRepo extends JpaRepository<WaitList,Long> {

    List<WaitList> findAllByTripId(Long tripId);
    List<WaitList> findAllByCustomerId(Long customerId);
    WaitList findByTripIdAndCustomerId(Long tripId, Long customerId);

    WaitList deleteById(Long id);
}
