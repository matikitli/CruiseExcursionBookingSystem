package com.repositories;

import com.models.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepo extends JpaRepository<Customer,Long>{

    //READ
    boolean existsByEmail(String email);

    boolean existsById( Long id);

    //CREATE

    //UPDATE

    //DELETE
    void deleteCustomerById(Long id);


}
