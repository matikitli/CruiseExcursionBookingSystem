package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

@EntityScan(
        basePackageClasses = {CruiseExcursionBookingApp.class, Jsr310JpaConverters.class}
)

@SpringBootApplication
public class CruiseExcursionBookingApp{

    public static void main(String[] args) {
        SpringApplication.run(CruiseExcursionBookingApp.class, args);
    }

}
