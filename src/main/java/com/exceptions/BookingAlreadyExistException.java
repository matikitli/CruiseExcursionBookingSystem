package com.exceptions;

public class BookingAlreadyExistException extends Exception {
    public BookingAlreadyExistException(String message){
        super("You can not book twice for the same trip. "+message);
    }
}
