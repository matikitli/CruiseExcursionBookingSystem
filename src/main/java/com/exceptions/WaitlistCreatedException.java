package com.exceptions;

public class WaitlistCreatedException extends Exception {
    public WaitlistCreatedException(Long message){
        super("You have been added to waitlist with id: "+message);
    }
}
