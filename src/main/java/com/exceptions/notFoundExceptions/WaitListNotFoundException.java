package com.exceptions.notFoundExceptions;

public class WaitListNotFoundException extends RuntimeException {

    public WaitListNotFoundException() {
    }

    public WaitListNotFoundException(String message) {
        super(message);
    }
}

