package com.exceptions.notFoundExceptions;

public class TripNotFoundException extends RuntimeException {
        public TripNotFoundException() {
        }

        public TripNotFoundException(String message) {
            super(message);
        }
    }


