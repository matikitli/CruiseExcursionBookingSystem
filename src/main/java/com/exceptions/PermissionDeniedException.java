package com.exceptions;

public class PermissionDeniedException extends Exception {

    public PermissionDeniedException(){
        super();
    }

    public PermissionDeniedException(String message){
        super(message);
    }
}
