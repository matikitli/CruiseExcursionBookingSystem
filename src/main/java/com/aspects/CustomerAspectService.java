package com.aspects;

import com.services.booking.BookingService;
import com.services.waitList.WaitListService;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class CustomerAspectService {

    @Autowired
    private BookingService bookingService;

    @Autowired
    private WaitListService waitListService;

    @Pointcut("execution(* com.services.customer.CustomerServiceImp.deleteExistingCustomer(..))")
    private static boolean deletingCustomer(){
        return true;
    }

    //INFO: deleting customer delete also all objects assigned to him.
    @Before(value = "deletingCustomer() && args(customerId,..)")
    private void beforeDeletingCustomerActions(Long customerId){

        bookingService.findBookingsByCustomerId(customerId).forEach(ele -> {
            try {
                bookingService.deleteBooking(ele.getId());
            } catch (Exception e){
                System.out.println(e.getMessage());
            }
        });

        waitListService.findAllByCustomerId(customerId).forEach(ele -> {
            try {
                waitListService.deleteWaitlistById(ele.getId());
            } catch (Exception e){
                System.out.println(e.getMessage());
            }
        });
    }

}
