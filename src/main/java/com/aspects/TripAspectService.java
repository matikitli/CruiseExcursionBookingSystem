package com.aspects;

import com.exceptions.notFoundExceptions.BookingNotFoundException;
import com.models.Booking;
import com.models.WaitList;
import com.services.booking.BookingService;
import com.services.waitList.WaitListService;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@Aspect
public class TripAspectService {

    @Autowired
    private BookingService bookingService;

    @Autowired
    private WaitListService waitListService;

    @Pointcut("execution(* com.services.trip.TripServiceImp.deleteTrip(..))")
    private static boolean deletingTrip(){
        return true;
    }

    //INFO: deleting trip also delete automatically all bookings assigned to it.
    @Before(value = "deletingTrip() && args(tripId,..)")
    private void beforeDeletingTripActions(Long tripId){

        StringBuilder response = new StringBuilder("All objects for trip: ")
                .append(tripId).append(" were deleted. Deleted bookings: [");

        List<Booking> bookingsToDelete = bookingService.findAllBookings().stream()
                .filter(booking -> booking.getTrip().getId().equals(tripId)).collect(Collectors.toList());

        for(Booking booking : bookingsToDelete){
            response.append("  ").append(booking.getId()).append("  ");
            try {
                bookingService.deleteBooking(booking.getId());
            } catch (BookingNotFoundException e){
                //protect AOP_trip from failing
            }
        }
        response.append("]. Deleted wait_lists: [");

        List<WaitList> waitListsToDelete = waitListService.findAll().stream()
                .filter(wl -> wl.getTripId().equals(tripId)).collect(Collectors.toList());


        for (WaitList wl : waitListsToDelete){
            try {
                response.append("  ").append(wl.getId()).append("  ");
                waitListService.deleteWaitlistById(wl.getId());
            } catch (BookingNotFoundException e){
                //protect AOP_trip from failing
            }
        }

        System.out.println(response.append("].").toString());
    }
}
