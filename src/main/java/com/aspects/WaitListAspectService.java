package com.aspects;

import com.controllers.booking.BookingRequest;
import com.controllers.trip.TripRequest;
import com.exceptions.BookingAlreadyExistException;
import com.exceptions.WaitlistCreatedException;
import com.models.Trip;
import com.models.WaitList;
import com.services.booking.BookingService;
import com.services.trip.TripService;
import com.services.waitList.WaitListService;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Aspect
public class WaitListAspectService {

    @Autowired
    private WaitListService waitListService;

    @Autowired
    private BookingService bookingService;

    @Autowired
    private TripService tripService;

    @Pointcut("execution(* com.services.booking.BookingServiceImp.deleteBooking(..))" +
            "|| execution(* com.services.booking.BookingServiceImp.editBooking(..))")
    private static boolean afterChangesToBookings(){
        return true;
    }

    @Pointcut("execution(* com.services.waitList.WaitListServiceImp.deleteWaitlistById(..))")
    private static boolean afterChangesToWaitlists(){
        return true;
    }

    //INFO: converts waitlists to bookings every time booking is deleted/edited and waitlist is edited
    @After("afterChangesToBookings() || afterChangesToWaitlists()")
    private void searchAndTransferPossibleWaitlistIntoBookings() throws BookingAlreadyExistException, WaitlistCreatedException {
        List<WaitList> allWaitlists = waitListService.findAll();
        for(WaitList waitList : allWaitlists){
            if(tripService.willFitOnTrip(waitList.getTripId(), waitList.getNumberOfSeats())){
                bookingService.createNewBooking(transformWaitlistIntoBookingRequest(waitList));
                waitListService.deleteWaitlistById(waitList.getId());
                System.out.println("Waitlist with id: "+waitList.getId()+" was transfered into booking for trip with id: "+waitList.getTripId());
            }
            else System.out.println("Waitlist with id: "+waitList.getId()+" wont fit into trip with id:"+waitList.getTripId()
                    +" and will wait for another changes.");
        }
    }

    private BookingRequest transformWaitlistIntoBookingRequest(WaitList waitList){
        Trip trip = tripService.findTripById(waitList.getTripId());
        return new BookingRequest(waitList.getCustomerId(), waitList.getNumberOfSeats(),
                new TripRequest(trip.getExcursion().getId(), trip.getDate()));
    }
}
