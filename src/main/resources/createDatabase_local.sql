#you must run this script not from intellij but mysql console
#if you have mysql installed go to CMD and type mysql --password
# (bc u already probably have some user for mysql created)
#You can change 'databaseAdmin' and 'password' to whatever you wanna
#But pls remember this credentials

CREATE DATABASE CruiseExcursionDatabase;
CREATE USER 'databaseAdmin'@'localhost' IDENTIFIED BY 'password';
GRANT ALL ON CruiseExcursionDatabase.* TO 'databaseAdmin'@'localhost';